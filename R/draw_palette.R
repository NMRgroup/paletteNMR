#' Draw a palette using 'nmr_colors'
#'
#' @inheritParams nmr_colors
#' @param ncols A number of columns to display in the drawn palette
#'
#' @return A grid of color swatches
#' @export
#'
#' @importFrom rlang .data
#'
#' @examples
#' draw_palette(scale = "numerical", pal = "nondiverg", set = "BT6", index = 1:4, ncols = 2)
#'
#' draw_palette(scale = "categorical", pal = "quad", set = "O")
#'
draw_palette <-
  function(scale = "categorical",
           pal = "quad",
           set = "O",
           index = NULL,
           pal_df = paletteNMR::nmrPalettes_2020,
           ncols = 1) {
    # Retrieves function args called with 'draw_palette'
    args <- get_pal_args(as.list(match.call()))
    # 'nmr_colors' has no 'ncols' argument
    args$ncols <- NULL

    output <- do.call(nmr_colors, args)

    ggplot2::ggplot(data = data.frame(hex = output),
                    ggplot2::aes(fill = .data$hex)) +
      ggplot2::facet_wrap(
        ggplot2::vars(.data$hex),
        ncol = ncols
      ) +
      ggplot2::geom_rect(
        ggplot2::aes(
          xmin = 1,
          xmax = 2,
          ymin = 1,
          ymax = 2
        )) +
      ggplot2::lims(
        x = c(1, 2),
        y = c(1, 2)
      ) +
      ggplot2::scale_fill_manual(values = as.character(output),
                                 guide = "none") +
      theme_nmr() +
      ggplot2::theme(
        axis.text = ggplot2::element_blank(),
        axis.ticks = ggplot2::element_blank(),
        panel.grid = ggplot2::element_blank(),
        panel.spacing = ggplot2::unit(0, "lines"),
        plot.margin = ggplot2::unit(c(0, 0, 0, 0), "lines"),
        strip.text = ggplot2::element_blank()
      )
  }

#' Helper function to select names used in calling `nmr_colors`
#'
#' @param arg_list A list of arguments used to call the function calling
#'   `get_pal_args`
#' @param colname_str A string selecting arguments by name (regex allowed)
#'
#' @return A subset of supplied argument names
#'
get_pal_args <- function(arg_list,
                         colname_str = "(^(scale|pal|set|index|contrast|discrete|interpolate|reverse))") {

  # Drop first argument, which is whatever function get_pal_args is called from
  arg_list[[1]] <- NULL

  checkHasNames(arg_list)
  # Select any columns matched by supplied string
  arg_list[grep(colname_str, names(arg_list), perl = TRUE)]
}

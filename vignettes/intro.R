## ----setup, echo=FALSE--------------------------------------------------------
library(paletteNMR)
suppressPackageStartupMessages({
  library(ggplot2)
})

## ---- sample_call-------------------------------------------------------------
output <- nmr_colors(scale = "categorical", pal = "quad", set = "F")

output

## ----raw_gg1------------------------------------------------------------------
data.frame(hex = output) |> 
  ggplot(aes(fill = hex)) +
  facet_wrap( ~ hex, ncol = 1) +
  geom_rect(aes(xmin = 1, xmax = 2,
                ymin = 1, ymax = 2)) +
  lims(x=c(1,2),y=c(1,2)) +
  scale_fill_manual(values = as.character(output),
                    guide = "none") +
  theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank(),
        panel.spacing = unit(-0.5, "lines"),
        plot.margin = unit(c(0,-0.25,0,-0.25), "lines"),
        strip.background = element_blank(),
        strip.text = element_blank())

## ----raw_gg2------------------------------------------------------------------
data.frame(hex = output) |> 
  ggplot(aes(fill = hex)) +
  facet_wrap( ~ hex, ncol = 1) +
  geom_rect(aes(xmin = 1, xmax = 2,
                ymin = 1, ymax = 2)) +
  lims(x=c(1,2),y=c(1,2)) +
  scale_nmr_fill_categorical(pal = "quad",
                             set = "F",
                             guide = "none") +
  theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank(),
        panel.spacing = unit(-0.5, "lines"),
        plot.margin = unit(c(0,-0.25,0,-0.25), "lines"),
        strip.background = element_blank(),
        strip.text = element_blank())

## ----draw_pal-----------------------------------------------------------------
draw_palette(scale = "numerical", "nondiverg", "RT5")


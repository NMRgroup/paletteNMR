# Testing the 'get_colors' function

library(paletteNMR)

context("Palette maker capabilities")

test_colors <- nmr_colors("categorical",
                          "hept",
                          "E",
                          pal_df = paletteNMR::nmrPalettes_2020)

test_palette_fn <- nmr_pal("categorical",
                           "hept",
                           "E",
                           pal_df = paletteNMR::nmrPalettes_2020)

index_vals <- c(1,3,5,7)
indexed_palette_fn <- nmr_pal("categorical",
                              "hept",
                              "E",
                              pal_df = paletteNMR::nmrPalettes_2020,
                              index = eval(index_vals))

rev_palette_fn <- nmr_pal("categorical",
                          "hept",
                          "E",
                          pal_df = paletteNMR::nmrPalettes_2020,
                          reverse = TRUE)

interp_palette_fn <- nmr_pal("categorical",
                             "hept",
                             "E",
                             pal_df = paletteNMR::nmrPalettes_2020,
                             interpolate = TRUE)

test_that("nmr_pal produces function taking single (integer) argument", {
  expect_is(test_palette_fn, "function")
  expect_is(test_palette_fn(5), "character")
  expect_is(test_palette_fn("5"), "character")
  expect_condition(test_palette_fn("5f"), message = "NA\\/NaN")
  # NA/NaN error thrown because fn attempts to call 1:"5f"
})

test_that("nmr_pal allows for reversing and indexing colors", {
  expect_identical(test_palette_fn(length(test_colors)),
                   rev(rev_palette_fn(length(test_colors))))
  expect_equal(indexed_palette_fn(length(index_vals)),
               as.vector(test_colors[index_vals]))
  expect_equal(indexed_palette_fn(length(index_vals) + 1)[length(index_vals) + 1],
               NA_character_)
})

test_that(paste0("Interpolated palette has at least two overlapping colors with",
                 "non-interpolated"), {
  expect_gte(length(union(interp_palette_fn(floor(length(test_colors)/2)),
                          test_palette_fn(floor(length(test_colors)/2)))), 2)
  expect_equal(interp_palette_fn(length(test_colors)),
               test_palette_fn(length(test_colors)))
})

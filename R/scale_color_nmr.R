#'Generate a categorical color scale with NMR colors
#'
#'@inheritParams nmr_colors
#'@param discrete A boolean toggling whether the aesthetic is discrete
#'@param interpolate  A boolean toggling whether to interpolate colors
#'@param reverse A boolean toggling whether to reverse the palette
#'@param ... Additional arguents to pass to `colorRampPalette`
#'
#'@seealso \code{\link{scale_nmr_color_diverging}},
#'  \code{\link{scale_nmr_color_sequential}},
#'  \code{\link{scale_nmr_color_orthogonal}}
#'
#'@return A color scale for use in ggplot2 graphics.
#'@export
#'
scale_nmr_color_categorical <- function(pal = "All",
                                        set = "",
                                        index = NULL,
                                        pal_df = paletteNMR::nmrPalettes_2020,
                                        discrete = TRUE,
                                        interpolate = FALSE,
                                        reverse = FALSE,
                                        ...) {
  # Based on drsimonj blog post
  # (https://drsimonj.svbtle.com/creating-corporate-colour-palettes-for-ggplot2)

  args <- get_pal_args(as.list(match.call()))
  # `nmr_pal` does not need discrete argument
  args$discrete <- NULL
  # Evaluate index so that actual numbers get passed
  args$index <- eval(index)
  # Specify palette arguments (implicit in function call)
  args$scale <- "categorical"

  cat_pal <- do.call(nmr_pal, args)

  if (discrete) {
    output <- ggplot2::discrete_scale("colour",
                                      paste0("nmr_", args$pal, args$set),
                                      palette = cat_pal, ...)
  }

  if (!discrete) {
    warning("Consider combining categories rather than interpolating colors")

    output <-
      ggplot2::scale_color_gradientn(colours = cat_pal(256)[!is.na(cat_pal(256))],
                                     ...)
  }

  output
}

#' Generate a diverging color scale with NMR colors
#'
#' @inheritParams nmr_colors
#' @param discrete A boolean toggling whether the aesthetic is discrete
#' @param interpolate  A boolean toggling whether to interpolate colors
#' @param reverse A boolean toggling whether to reverse the palette
#' @param ... Additional arguents to pass to `colorRampPalette`
#'
#'@seealso \code{\link{scale_nmr_color_sequential}},
#'  \code{\link{scale_nmr_color_orthogonal}},
#'  \code{\link{scale_nmr_color_categorical}}
#'
#' @return A color scale for use in ggplot2 graphics.
#' @export
#'
scale_nmr_color_diverging <- function(set = "",
                                      index = NULL,
                                      pal_df = paletteNMR::nmrPalettes_2020,
                                      discrete = TRUE,
                                      interpolate = FALSE,
                                      reverse = FALSE,
                                      ...) {
  # Based on drsimonj blog post
  # (https://drsimonj.svbtle.com/creating-corporate-colour-palettes-for-ggplot2)

  args <- get_pal_args(as.list(match.call()))

  if (!is.null(args$pal)) {
    if (args$pal != "diverg") {
      stop(
        paste(
          "Do not specify a 'pal'.",
          "This function implicitly uses pal = 'diverg'.",
          "Maybe you meant to specify a 'set'?"
        )
      )
    }
  }

  # `nmr_pal` does not need discrete argument
  args$discrete <- NULL
  # Evaluate index so that actual numbers get passed
  args$index <- eval(index)
  # Specify palette arguments (implicit in function call)
  args$scale <- "numerical"
  args$pal <- "diverg"

  num_pal <- do.call(nmr_pal, args)

  if (discrete) {
    output <- ggplot2::discrete_scale("colour",
                                      paste0("nmr_", args$pal, args$set),
                                      palette = num_pal, ...)
  }

  if (!discrete) {
    output <-
      ggplot2::scale_color_gradientn(colours = num_pal(256)[!is.na(num_pal(256))],
                                     ...)
  }

  output
}

#' Generate a sequential color scale with NMR colors
#'
#' @inheritParams nmr_colors
#' @param discrete A boolean toggling whether the aesthetic is discrete
#' @param interpolate  A boolean toggling whether to interpolate colors
#' @param reverse A boolean toggling whether to reverse the palette
#' @param ... Additional arguents to pass to `colorRampPalette`
#'
#'@seealso \code{\link{scale_nmr_color_diverging}},
#'  \code{\link{scale_nmr_color_orthogonal}},
#'  \code{\link{scale_nmr_color_categorical}}
#'
#' @return A color scale for use in ggplot2 graphics.
#' @export
#'
scale_nmr_color_sequential <- function(set = "",
                                       index = NULL,
                                       pal_df = paletteNMR::nmrPalettes_2020,
                                       discrete = TRUE,
                                       interpolate = FALSE,
                                       reverse = FALSE,
                                       ...) {
  # Based on drsimonj blog post
  # (https://drsimonj.svbtle.com/creating-corporate-colour-palettes-for-ggplot2)

  args <- get_pal_args(as.list(match.call()))

  if (!is.null(args$pal)) {
    if (args$pal != "nondiverg") {
      stop(
        paste(
          "Do not specify a 'pal'.",
          "This function implicitly uses pal = 'nondiverg'.",
          "Maybe you meant to specify a 'set'?"
        )
      )
    }
  }

  # `nmr_pal` does not need discrete argument
  args$discrete <- NULL
  # Evaluate index so that actual numbers get passed
  args$index <- eval(index)
  # Specify palette arguments (implicit in function call)
  args$scale <- "numerical"
  args$pal <- "nondiverg"

  num_pal <- do.call(nmr_pal, args)

  if (discrete) {
    output <- ggplot2::discrete_scale("colour",
                                      paste0("nmr_", args$pal, args$set),
                                      palette = num_pal, ...)
  }

  if (!discrete) {
    output <-
      ggplot2::scale_color_gradientn(colours = num_pal(256)[!is.na(num_pal(256))],
                                     ...)
  }

  output
}

#' Generate a orthogonal color scale with NMR colors
#'
#' @inheritParams nmr_colors
#' @param discrete A boolean toggling whether the aesthetic is discrete
#' @param interpolate  A boolean toggling whether to interpolate colors
#' @param reverse A boolean toggling whether to reverse the palette
#' @param ... Additional arguents to pass to `colorRampPalette`
#'
#'@seealso \code{\link{scale_nmr_color_diverging}},
#'  \code{\link{scale_nmr_color_sequential}},
#'  \code{\link{scale_nmr_color_categorical}}
#'
#' @return A color scale for use in ggplot2 graphics.
#' @export
#'
scale_nmr_color_orthogonal <- function(set = "",
                                       index = NULL,
                                       pal_df = paletteNMR::nmrPalettes_2020,
                                       discrete = TRUE,
                                       interpolate = FALSE,
                                       reverse = FALSE,
                                       ...) {
  # Based on drsimonj blog post
  # (https://drsimonj.svbtle.com/creating-corporate-colour-palettes-for-ggplot2)

  args <- get_pal_args(as.list(match.call()))

  if (!is.null(args$pal)) {
    if (args$pal != "ortho") {
      stop(
        paste(
          "Do not specify a 'pal'.",
          "This function implicitly uses pal = 'ortho'.",
          "Maybe you meant to specify a 'set'?"
        )
      )
    }
  }

  # `nmr_pal` does not need discrete argument
  args$discrete <- NULL
  # Evaluate index so that actual numbers get passed
  args$index <- eval(index)
  # Specify palette arguments (implicit in function call)
  args$scale <- "numerical"
  args$pal <- "ortho"

  num_pal <- do.call(nmr_pal, args)

  if (discrete) {
    output <- ggplot2::discrete_scale("colour",
                                      paste0("nmr_", args$pal, args$set),
                                      palette = num_pal, ...)
  }

  if (!discrete) {
    output <-
      ggplot2::scale_color_gradientn(colours = num_pal(256)[!is.na(num_pal(256))],
                                     ...)
  }

  output
}

#' Generate a colour scale for nmr colors
#'
#' @rdname  scale_nmr_color_categorical
#' @export
#'
scale_nmr_colour_categorical <- function(pal = "All",
                                         set = "",
                                         index = NULL,
                                         pal_df = paletteNMR::nmrPalettes_2020,
                                         discrete = TRUE,
                                         interpolate = FALSE,
                                         reverse = FALSE,
                                         ...) {
  scale_nmr_color_categorical(
    pal_df = pal_df,
    pal = pal,
    set = set,
    index = index,
    discrete = discrete,
    interpolate = interpolate,
    reverse = reverse,
    ...
  )
}

#' Generate a colour scale for nmr colors
#'
#' @rdname  scale_nmr_color_diverging
#' @export
#'
scale_nmr_colour_diverging <- function(set = "",
                                       index = NULL,
                                       pal_df = paletteNMR::nmrPalettes_2020,
                                       discrete = TRUE,
                                       interpolate = FALSE,
                                       reverse = FALSE,
                                       ...) {
  scale_nmr_color_diverging(
    pal_df = pal_df,
    set = set,
    index = index,
    interpolate = interpolate,
    reverse = reverse,
    ...
  )
}

#' Generate a colour scale for nmr colors
#'
#' @rdname  scale_nmr_color_sequential
#' @export
#'
scale_nmr_colour_sequential <- function(set = "",
                                        index = NULL,
                                        pal_df = paletteNMR::nmrPalettes_2020,
                                        discrete = TRUE,
                                        interpolate = FALSE,
                                        reverse = FALSE,
                                        ...) {
  scale_nmr_color_sequential(
    pal_df = pal_df,
    set = set,
    index = index,
    discrete = discrete,
    interpolate = interpolate,
    reverse = reverse,
    ...
  )
}

#' Generate a colour scale for nmr colors
#'
#' @rdname  scale_nmr_color_orthogonal
#' @export
#'
scale_nmr_colour_orthogonal <- function(set = "",
                                        index = NULL,
                                        pal_df = paletteNMR::nmrPalettes_2020,
                                        discrete = TRUE,
                                        interpolate = FALSE,
                                        reverse = FALSE,
                                        ...) {
  scale_nmr_color_orthogonal(
    pal_df = pal_df,
    set = set,
    index = index,
    discrete = discrete,
    interpolate = interpolate,
    reverse = reverse,
    ...
  )
}

#' Generate a palette function
#'
#' @inheritParams nmr_colors
#' @param interpolate  A boolean toggling whether to interpolate colors
#' @param reverse A boolean toggling whether to reverse the palette
#' @param ... Additional arguents to pass to `colorRampPalette`
#'
#' @return A (sub)set of colors from an NMR palette, or a set interpolated from
#'   one
#' @export
#'
#' @importFrom grDevices colorRampPalette
nmr_pal <- function(scale = "Categorical",
                    pal = "quad",
                    set = "C",
                    index = NULL,
                    pal_df = paletteNMR::nmrPalettes_2020,
                    interpolate = FALSE,
                    reverse = FALSE,
                    ...) {
  # Based on drsimonj blog post
  # (https://drsimonj.svbtle.com/creating-corporate-colour-palettes-for-ggplot2)

  # Retrieves function args called with 'nmr_pal'
  args <- get_pal_args(as.list(match.call()))
  # Removes arguments unnecessary for 'nmr_colors'
  args[c("interpolate", "reverse")] <- NULL
  # Evaluate index so that actual numbers get passed
  args$index <- eval(index)

  cols <- do.call(nmr_colors, args)

  cols <- as.vector(cols)

  if (reverse)
    cols <- rev(cols)

  if (interpolate) {
    # Return an interpolated palette
    colorRampPalette(cols, ...)
  } else {
    # Return the discrete colors selected
    function(n) {
      cols[1:n]
    }
  }
}


<!-- README.md is generated from README.Rmd. Please edit that file -->

# paletteNMR <img src="man/figures/logo.PNG" align="right" alt="" width="100" />

paletteNMR provides functions to access and browse the set of palettes
available for use in publications at NMR.

The main palette set is NMR’s internal one, but the package also
contains palettes for use with other clients’ projects / color schemes,
and can be expanded to include other palettes as needed. See a mirror of
this info at
[r-universe/paletteNMR](https://nmrgroup.r-universe.dev/ui#package:paletteNMR)

Package currently includes:

| Palette name        | Description                                                     |
|:--------------------|:----------------------------------------------------------------|
| nmrColors_2020      | NMR color swatches, 2020                                        |
| nmrPalettes_2020    | NMR palettes, 2020                                              |
| neeaPalettes_2017   | Slim NEEA Market Research & Evaluation color palette, from 2017 |
| nexantPalettes_2017 | Slim Nexant color palette                                       |

## Installation

``` r
# Enable repository from nmrgroup
options(repos = c(
  nmrgroup = 'https://nmrgroup.r-universe.dev',
  CRAN = 'https://cloud.r-project.org'))

# Download and install paletteNMR in R
install.packages('paletteNMR')

# Browse the paletteNMR manual pages
help(package = 'paletteNMR')
```

Follow our package builds on the [NMR
r-universe](https://nmrgroup.r-universe.dev/ui#package:paletteNMR)

## Code of Conduct

Please note that the paletteNMR project is released with a [Contributor
Code of
Conduct](https://contributor-covenant.org/version/2/0/CODE_OF_CONDUCT.html).
By contributing to this project, you agree to abide by its terms.

### Logo

Adapted from the original icon
<a href="https://thenounproject.com/term/color-palette/968506/" target="_blank">“color
palette”</a> by Jemis mali, licensed as Creative Commons CCBY.

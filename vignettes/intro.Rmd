---
title: "Intro to paletteNMR"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Intro to paletteNMR}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r setup, echo=FALSE}
library(paletteNMR)
suppressPackageStartupMessages({
  library(ggplot2)
})
```


This package is intended to be used for storing and accessing NMR color palettes in R, for easy integration in visualization and reporting. 

At the moment, it contains three core functions. The first, `nmr_colors`, produces a named list of colors (or, optionally) a contrasting set of colors. Use `?nmr_colors` to see further documentation. The second, `draw_palette`, is a thin wrapper on `nmr_colors` for drawing ggplot figures using the palettes.

Then there are additional helper functions, including `scale_*` functions that integrate NMR palettes with ggplot2

### Navigating existing palettes

First, a high-level view of the palettes available in the NMR set of palettes. At the top layer, there are two scales, `categorical` and `numerical`. Each scale contains various palettes, detailed below. And most palettes contain multiple sets, e.g. varying the base color.

*See below for details on what sets to consider in specific applications (e.g. high contrast, B&W printing, etc.)*

#### Categorical

For maximum legibility, **you should have five or fewer categories**. If possible, consider combining categories. Scales include:

- Pairs 
- Quads
- Map (for points and polygons)
- Lines
- hex and Hept

##### Quads

Among the quads, any consecutive subset of colors in these sets could be used since they maintain high contrast as indicated by the B&W variants. Quads C, F and H contain distinct colors, allowing them to be used on graphs of different attributes. Quad C or Hept E are best in case of B&W printing.

##### Lines

The line palette maximizes legibility when printed in B&W, but avoid having more than 4 categories in one line chart. Consider using unique line types or point markers if possible.

##### Maps

To improve legibility, you should use different shapes as well as colors for different category markers. Using a color to show water is optional--usually not needed (and best omitted)

#### Numerical

##### Non-divergent

Do not use the green and red sets side-by-side. If you need a lighter scale, select a longer sequence than you need and omit the darkest color. As with categorical palettes, **avoid having more than five categories**.

Avoid interpolating colors unless you need a continuous scale. If you do need continuous scale, use the viridis scale built into the `ggplot2` package (see `?scale_color_viridis_d`)

You can join a non-divergent series to itself with light colors on the end to accentuate a mean (i.e. the inverse of the divergent scale below)

##### Divergent

If you need a continuous scale, consider using the BrBG ramp from ColorBrewer, also in the `ggplot2` package (see `?scale_color_brewer`)

### Accessing existing palettes

Calling `nmr_colors` returns the color IDs and their hex codes.

```{r, sample_call}
output <- nmr_colors(scale = "categorical", pal = "quad", set = "F")

output
```

Create graphics using above call to `nmr_colors` with the following code:

```{r raw_gg1}
data.frame(hex = output) |> 
  ggplot(aes(fill = hex)) +
  facet_wrap( ~ hex, ncol = 1) +
  geom_rect(aes(xmin = 1, xmax = 2,
                ymin = 1, ymax = 2)) +
  lims(x=c(1,2),y=c(1,2)) +
  scale_fill_manual(values = as.character(output),
                    guide = "none") +
  theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank(),
        panel.spacing = unit(-0.5, "lines"),
        plot.margin = unit(c(0,-0.25,0,-0.25), "lines"),
        strip.background = element_blank(),
        strip.text = element_blank())
```

paletteNMR also contains `scale_*` functions modeled on those in the `ggplot2` package for visualization. 

```{r raw_gg2}
data.frame(hex = output) |> 
  ggplot(aes(fill = hex)) +
  facet_wrap( ~ hex, ncol = 1) +
  geom_rect(aes(xmin = 1, xmax = 2,
                ymin = 1, ymax = 2)) +
  lims(x=c(1,2),y=c(1,2)) +
  scale_nmr_fill_categorical(pal = "quad",
                             set = "F",
                             guide = "none") +
  theme(axis.text = element_blank(),
        axis.ticks = element_blank(),
        panel.grid = element_blank(),
        panel.spacing = unit(-0.5, "lines"),
        plot.margin = unit(c(0,-0.25,0,-0.25), "lines"),
        strip.background = element_blank(),
        strip.text = element_blank())
```

### Drawing palettes (draw_palette)

```{r draw_pal}
draw_palette(scale = "numerical", "nondiverg", "RT5")
```

See the "palette_browse.Rmd" vignette to browse available palettes.
